
---
-- TODO: Doc.
--
-- @classmod kthread.Thread

local minser = require "minser"

local klass = require "klass"
local Thread = klass:extend("kthread.Thread")

local threads = setmetatable({ }, { __mode="kv" })

---
-- TODO: Doc.
--
Thread.input = nil

---
-- TODO: Doc.
--
Thread.output = nil

---
-- TODO: Doc.
--
Thread.thread = nil

---
-- TODO: Doc.
--
Thread.alive = false

---
-- Constructor.
--
function Thread:init()
	self.input = love.thread.newChannel()
	self.output = love.thread.newChannel()
	self.thread = love.thread.newThread([[
		local function main(name, input, output, ...)
			local C = require(name)
			local inst = setmetatable({ }, C)
			inst.input = input
			inst.output = output
			inst.alive = true
			return inst:run(...)
		end
		return main(...)
	]])
	threads[self.thread] = self
end

---
-- TODO: Doc.
--
function Thread:threaderror(err)
	self = threads[self]
	if self then
		return self:onerror(err)
	end
end

rawset(love, "threaderror", Thread.threaderror)

---
-- TODO: Doc.
--
-- @tparam any ...
function Thread:start(...)
	return self.thread:start(self.__name, self.output, self.input, ...)
end

---
-- TODO: Doc.
--
function Thread:stop()
	return self.output:push("stop")
end

---
-- TODO: Doc.
--
-- @tparam any ...
function Thread:run(...) -- luacheck: ignore
end

---
-- TODO: Doc.
--
-- @treturn boolean alive
function Thread:isalive()
	if self.thread then
		return self.thread:isRunning()
	else
		return self.alive
	end
end

---
-- TODO: Doc.
--
function Thread:wait()
	return assert(self.thread):wait()
end

---
-- TODO: Doc.
--
-- @treturn any ...
function Thread:receive()
	local what = self.input:pop()
	if what == "stop" then
		self.alive = false
		return self:receive()
	elseif what == "data" then
		local data = self.input:demand()
		local function bail(ok, ...)
			assert(ok, ...)
			return ...
		end
		return bail(minser.load(data))
	end
end

---
-- TODO: Doc.
--
-- @tparam any ...
function Thread:send(...)
	self.output:push("data")
	return self.output:push(assert(minser.dump(...)))
end

---
-- TODO: Doc.
--
-- @tparam string err
function Thread:onerror(err) -- luacheck: ignore
	return error(err)
end

return Thread
